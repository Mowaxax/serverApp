package fr.univtln.m2dapm.mbernard232;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import org.glassfish.grizzly.http.server.HttpServer;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

/**
 * REST server
 */
public class RestServer
{
    /**
     * Get the port from environment, otherwise fall back to default port.
     *
     * @param defaultPort
     *              The default port to use if no port was found in the environment.
     * @return
     *          The port to use if found in the environment, the default port otherwise.
     */
    private static int getPort(int defaultPort) {
        String httpPort = System.getProperty("jersey.server.port");
        if (null != httpPort) {
            try {
                return Integer.parseInt(httpPort);
            } catch (NumberFormatException e) {
            }
        }
        return defaultPort;
    }

    /**
     * Get the IP from environment, otherwise fall back to default IP.
     *
     * @param defaultIp
     *              The default IP to use if no IP was found in the environment.
     * @return
     *          The IP to use if found in the environment, the default IP otherwise.
     */
    private static String getIp(String defaultIp) {
        String httpIp = System.getProperty("jersey.server.ip");
        if (null != httpIp) {
            defaultIp = httpIp;
        }
        return defaultIp;
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri(getIp("http://localhost/")).port(getPort(9998)).build();
    }

    public static final URI BASE_URI = getBaseURI();

    protected static HttpServer startServer() throws IOException {
        ResourceConfig resourceConfig = new PackagesResourceConfig("fr.univtln.m2dapm.mbernard232");
        return GrizzlyServerFactory.createHttpServer(BASE_URI, resourceConfig);
    }

    public static void main(String[] args) throws IOException {
        HttpServer httpServer = startServer();
        System.in.read();
        httpServer.stop();
    }
}
