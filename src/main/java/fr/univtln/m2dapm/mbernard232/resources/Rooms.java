package fr.univtln.m2dapm.mbernard232.resources;

import fr.univtln.m2dapm.mbernard232.item.Item;
import fr.univtln.m2dapm.mbernard232.room.Room;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/rooms")
public class Rooms {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlD35");
    //@PersistenceContext
    private static EntityManager em = emf.createEntityManager();

    static {
        Room room1 = new Room(123456789, "Main room", "Aix");
        Room room2 = new Room(987654321, "Secondary room", "Aix");
        Item item1 = new Item(123, "Nutella");
        Item item2 = new Item(321, "Cacao");
        room1.addItem(item1, 5);
        room2.addItem(item2, 5);

        EntityTransaction transac = em.getTransaction();
        transac.begin();
//        try {
            em.persist(item1);
            em.persist(item2);
            em.persist(room1);
            em.persist(room2);
            transac.commit();
//        } catch (Exception e) {
//            transac.rollback();
//        }
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getRooms() {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Room> cq = cb.createQuery(Room.class);
        Root<Room> rootEntry = cq.from(Room.class);
        CriteriaQuery<Room> all = cq.select(rootEntry);
        TypedQuery<Room> allQuery = em.createQuery(all);
        List<Room> rooms = allQuery.getResultList();

        return Response.ok("Found rooms : " + rooms + "\n").build();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{room_id: [0-9]+}")
    public Response getRoom(@PathParam("room_id") long roomID) {
        Room room = em.find(Room.class, roomID);
        return Response.ok("Found room : " + room +".\n").build();
    }

    @PUT
    @Path("/{room_id: [0-9]+}")
    public Response addRoom(@PathParam("room_id") long roomID) {
        Room room = new Room(roomID);

        EntityTransaction transac = em.getTransaction();
        transac.begin();
        em.persist(room);
        transac.commit();

        return Response.ok("You created the room " + room +".\n").build();
    }
}
