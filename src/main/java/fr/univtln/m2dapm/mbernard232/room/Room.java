package fr.univtln.m2dapm.mbernard232.room;

import fr.univtln.m2dapm.mbernard232.item.Item;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "ROOMS")
@XmlRootElement
public class Room implements Serializable{

    /**
     * The Room ID
     */
    @Id
    @Column(name = "ID")
    @XmlElement
    private long id;

    /**
     * The Room name
     */
    @Column(name = "NAME")
    @XmlElement
    private String name;

    /**
     * The Room location
     */
    @Column(name = "LOCATION")
    @XmlElement
    private String location;

    /**
     * The Items in the room, with their quantity
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "ITEM_QUANTITIES")
    //@MapKeyColumn(name = "ITEM_NAME")
    @MapKeyJoinColumn(name = "ITEM_ID")
    //@MapKeyEnumerated(EnumType.STRING)
    @Column(name = "QUANTITY")
    //@ManyToMany(cascade = CascadeType.ALL)
    @XmlElement
    private Map<Item, Integer> items;

    /**
     * Default constructor, used by JPA
     */
    public Room() {
        //Nothing to do
    }

    /**
     * Constructor
     *
     * @param id
     *              The ID of the room
     */
    public Room(long id) {
        this(id, null, null);
    }

    /**
     * Full constructor
     *
     * @param id
     *              The ID of the room
     * @param name
     *              The name of the room
     * @param location
     *              The location of the room
     */
    public Room(long id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Map<Item, Integer> getItems() {
        return items;
    }

    public void setItems(Map<Item, Integer> items) {
        this.items = items;
    }

    public void addItem(Item item, int quantity) {
        if(items == null) items = new HashMap<>();
        if(items.containsKey(item)) quantity += items.get(item);
        if(item.getRoom() != this) item.setRoom(this);
        items.put(item, quantity);
    }

    public void addItems(Map<Item, Integer> itemsToAdd) {
        for(Map.Entry<Item, Integer> entry : itemsToAdd.entrySet()) {
            addItem(entry.getKey(), entry.getValue());
        }
    }

    public void removeItem(Item item) {
        if(items != null) items.remove(item);
    }

    public void removeItem(Item item, int quantity) {
        if(items != null) {
            if(items.containsKey(item)) quantity -= items.get(item);
            if(quantity < 0) quantity = 0;
            items.put(item, quantity);
        }
    }

    public void removeItems(Map<Item, Integer> itemsToAdd) {
        for(Map.Entry<Item, Integer> entry : itemsToAdd.entrySet()) {
            removeItem(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", items=" + items +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return id == room.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
