package fr.univtln.m2dapm.mbernard232.item;

import fr.univtln.m2dapm.mbernard232.room.Room;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "ITEMS")
@XmlRootElement
public class Item implements Serializable {

    /**
     * The Item ID
     */
    @Id
    @Column(name = "ID")
    @XmlElement
    private long id;

    /**
     * The Item name
     */
    @Column(name = "NAME")
    @XmlElement
    private String name;

    /**
     * The Room in which the Item is stored
     */
    private Room room;

    /**
     * Default constructor, used by JPA
     */
    public Item() {
        //Nothing to do
    }

    /**
     * Constructor
     *
     * @param id
     *              The Item's ID
     * @param name
     *              The Item's name
     */
    public Item(long id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Full constructor
     *
     * @param id
     *              The Item's ID
     * @param name
     *              The Item's name
     * @param room
     *              The Room in which the Item is stored
     */
    public Item(long id, String name, Room room) {
        this.id = id;
        this.name = name;
        this.room = room;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        if(this.room != room) {
            if(this.room != null) this.room.removeItem(this);
            this.room = room;
        }
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
